*Version 2020-Fall-1.2, 31 August 2020*

# THIS IS A DRAFT. IT MAY CHANGE BEFORE THE FIRST CLASS ON 2 SEPTEMBER 2020.

### *CS-343 01 &mdash; Fall 2020*

# CS-343 Software Construction, Design and Architecture

## Credit and Contact Hours
3 credits<br>
Lecture: 3 hours/week

## Catalog Course Description
> *Software construction techniques and tools, software architectures and frameworks, design patterns, object-oriented design and programming. Efficiency, reliability and maintainability of software.*

## Instructor
Dr. Karl R. Wurst<br>
See <a href="http://cs.worcester.edu/kwurst/" target="_blank">http://cs.worcester.edu/kwurst/</a> for contact information and schedule.

## Meeting Times and Locations
Meets on Discord, MW 12:30-1:45pm

## It's in the Syllabus
<img src=http://www.phdcomics.com/comics/archive/phd051013s.gif><br>
<a href="http://www.phdcomics.com/comics.php?f=1583">http://www.phdcomics.com/comics.php?f=1583</a>

**If you don't find the answer to your question in the syllabus, then please ask me.**

## Textbook
<table cellpadding="1" border="0">
<tbody>
<tr>
</tr>
<td align="center"><a href="https://services.acm.org/public/qj/quickjoin/qj_control.cfm?promo=PWEBTOP&form_type=Student"><img width="700" alt="ACM logo" src="https://www.acm.org/images/top-menu/acm_logo_tablet.svg" /></a>
</td>
<td>This course requires an ACM Student Membership. For the <a href="https://services.acm.org/public/qj/quickjoin/qj_control.cfm?promo=PWEBTOP&form_type=Student">$19 student membership</a>, students get access to <a href="https://www.oreilly.com/online-learning/features.html">O'Reilly Online Learning</a> which includes hundreds of textbooks, videos, and training courses.<br><br>We will be using multiple learning resources from O'Reilly Online Learning for topics in this course.<br><br>The ACM Student Membership is good for a year. It will be used for CS-443 and CS-448 in Spring 2021 as well.
</td>
<tr>
<td align="center"><a title="By Michael Reschke (self-made, following OERCommons.org&#039;s example) [Public domain], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3AOERlogo.svg"><img width="100" alt="OERlogo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/OERlogo.svg/256px-OERlogo.svg.png"/></a>
</td>
<td>We will also be using freely available learning resources for topics in this course.
</td>
</tr>
</tbody>
</table>

## Required Materials
In addition to the textbook, to successfully complete this course you will need:

1.	**Laptop Computer:** You will need a laptop computer that you can bring to class sessions and can use at home. **You must bring your laptop to every class session.** The brand and operating system (Windows, Mac OS X, Linux) is unimportant – the software we will be using runs on all major operating systems and can be downloaded for free. (Chromebooks are not suitable for this class, and will not run the software we will using.) It is expected that you will download and install required software as needed over the course of the semester. 
2. **Internet Access:** You will need Internet access for access to:
	1.	**Blackboard** – All course materials and announcements will be made available through the course site on Blackboard. Students will be required to use Blackboard as the course tool and be familiar with uploading files.
	2. **WSU Gmail** &mdash; You must check your WSU Gmail account on a regular basis. All communications to the class, such as corrections to problem sets or changes in due dates, will be sent to your WSU Gmail account.
	3. **Discord** &mdash; We will use Discord for our synchronous, online class sessions.
	4. **GitLab** – We will use GitLab to host our projects.
	5.	**Tutorials and articles** – I will suggest, and you will research on your own, tutorials and articles for you to learn new technologies and techniques we need.

## Where Does This Course Lead?
* CS 348 Software Process Management
* CS 448 Software Development Capstone
* Your professional career

## Course Workload Expectations
***This is a three-credit course. You should expect to spend, on average, 9 hours per week on this class.***

You will spend 3 hours per week in class. In addition, you should expect to spend, on average, at least 6 hours per week during the semester outside of class. (See *Definition of the Credit Hour*)

## Definition of the Credit Hour
>Federal regulation defines a credit hour as an amount of work represented in intended learning outcomes and verified by evidence of student achievement that is an institutional established equivalence that reasonably approximates not less than –

>1.	One hour of classroom or direct faculty instruction and a minimum of two hours of out of class student work each week for approximately fifteen weeks for one semester or trimester hour of credit, or ten to twelve weeks for one quarter hour of credit, or the equivalent amount of work over a different amount of time; or 
2.	At least an equivalent amount of work as required in paragraph (1) of this definition for other academic activities as established by the institution including laboratory work, internships, practica, studio work, and other academic work leading to the award of credit hours.

>---New England Association of Schools and Colleges, Commission on Institutions of Higher Education, [Policy on Credits and Degrees](http://cihe.neasc.org/downloads/POLICIES/Pp111_PolicyOnCreditsAndDegrees.pdf)

## Prerequisites
This course has a prerequisite of CS 286 – Database Design and Applications. I expect that you understand the fundamentals of database structure and queries.

This course has a prerequisite of CS 348 - Software Process Management. I expect that you have had experience using a version control system to fork/clone, add/commmit, and push/pull, and how to use a build system. It has prerequisite of CS-140, and so I expect that you are competent at writing programs that contain moderate numbers of classes, that you can write and execute simple unit tests, write clear, well documented code, and use an appropriate development environment. 

***If you are missing any of this background, you should not take this course.***

## Course-Level Student Learning Outcomes
Upon successful completion of this course, students will be able to:

* Apply a wide variety of software construction techniques and tools, including state-based and table-­driven approaches to low-­level design of software
* Take requirements for simple systems and develop software architectures and high-­level designs
* Apply a variety of design patterns, frameworks, and architectures in designing a wide variety of software
* Perform object-­oriented design and programming with a high level of proficiency
* Analyze and modify software in order to improve its efficiency, reliability, and maintainability

## LASC Student Learning Outcomes
This course does not fulfill any LASC Content Area requirements, but contributes to the following Overarching Outcomes of LASC:

* Demonstrate effective oral and written communication.
* Employ quantitative and qualitative reasoning.
* Apply skills in critical thinking.
* Apply skills in information literacy.
* Understand the roles of science and technology in our modern world.
* Understand how scholars in various disciplines approach problems and construct knowledge.
* Display socially responsible behavior and act as socially responsible agents in the world.
* Make connections across courses and disciplines.

## Software Development Concentration Student Learning Outcomes
This course addresses the following outcomes of the Software Development Concentration of the Computer Science Major:

Graduates of the Software Development Concentration will be able to (in addition to the Computer Science Major Program Learning Outcomes):

1.	Work with stakeholders to specify, design, develop, test, modify, and document a software system. (Emphasis/Mastery)
2.	Organize, plan, follow, and improve on, appropriate software development methodologies and team processes for a software project. (Introduction)
3.	Evaluate, select, and use appropriate tools for source code control, build, test, deployment, and documentation management. (Introduction)
4.	Evaluate, select, and apply appropriate testing techniques and tools, develop test cases, and perform software reviews. (Introduction)
5.	Apply professional judgement, exhibit professional behavior, and keep skills up-to-date. (Mastery)

## Program-Level Student Learning Outcomes
This course addresses the following outcomes of the Computer Science Major (see [http://www.worcester.edu/ComputerScienceDept/Shared%20Documents/ProgramOutcomes.aspx]()):

Upon successful completion of the Major in Computer Science, students will be able to:

1.	Analyze a problem, develop/design multiple solutions and evaluate and document the solutions based on the requirements. (Emphasis)
2.	Communicate effectively both in written and oral form. (Emphasis)
4. Demonstrate an understanding of and appreciation for the importance of negotiation, effective work habits, leadership, and good communication with teammates and stakeholders. (Emphasis)
5. Learn new models, techniques, and technologies as they emerge and appreciate the necessity of such continuing professional development. (Emphasis)

## Course Topics
The topics covered in this course will be selected from (not all will be covered):

-   Design Principles
	-   Object Oriented Programming
		-   Abstraction
		-   Encapsulation
		-   Polymorphism
		-   Inheritance
    -   SOLID
    	-   Single Responsibility Principle (SRP)
		-   Open-Closed Principle (OCP)
		-   Liskov Substitution Principle (LSP)
		-   Interface Segregation Principle (ISP)
		-   Dependency Inversion Principle (DIP)
	-   DRY (Don't Repeat Yourself)
	-   YAGNI (You Ain't Gonna Need It)
	-   GRASP (General Responsibility Assignment Software Patterns)
		-   Controller
		-   Creator
		-   High Cohesion
		-   Indirection
		-   Information Expert
		-   Low Coupling
		-   Polymorphism
		-   Protected Variations
		-   Pure Fabrication
	-   "Encapsulate what varies."
	-   "Program to an interface, not an implementation."
	-   "Favor composition over inheritance."
	-   "Strive for loosely coupled designs between objects that interact"
	-   Principle of Least Knowledge (AKA Law of Demeter)
	-   Inversion of Control
-	Design Patterns
    -   Creational
    -   Structural
    -   Behavioral
    -   Concurrency
-   Refactoring
-   Smells
	-   Code Smells
	-   Design Smells
-   Software Architectures
	-   Architectural Patterns
	-   Architectural Styles
-	REST API Design
-   Software Frameworks
-   Documentation
-   Modeling
	-   Unified Modeling Language (UML)
	-   C4 Model
-   Anti-Patterns
-	Implementation of Web Systems
	- Front end
	- Back end
	- Data persistence layer

## Instructional Methods
This class will not be a traditional “lecture” class, and will incorporate some teaching methods that may be unfamiliar to you.

### POGIL
Rather than lecturing about the course content, you and your classmates will "discover" the content for yourselves through small-group work.

The group work will be a very structured style called Process Oriented Guided Inquiry Learning (POGIL). Through investigation of models of the concepts and answering questions that guide the team toward understanding of the models, your team will learn both the content and team process skills. In your POGIL groups each group member will have a specific role to play during the activity, and roles will be rotated so that everyone will get to experience a variety of process skills.

For more information on POGIL, see [https://pogil.org/about-pogil/what-is-pogil](https://pogil.org/about-pogil/what-is-pogil).

### Competency- and Specification-Based Grading
See *Grading Policies* below.

## Grading Policies
I want everyone receiving a passing grade in this course to be, at least, minimally competent in the course learning outcomes and for that to be reflected in your course grade. Traditional grading schemes do a poor job of indicating competency.

As an example, imagine a course with two major learning outcomes: X and Y. It is widely considered that a course grade of C indicates that a student is minimally competent in achieving the course outcomes. However, if the student were to receive a grade of 100 for outcome X, and a grade of 40 for outcome Y, the student would still have a 70 (C-) average for the course. Yet the student is clearly not competent in outcome Y.

Therefore the grading in this course will be handled in a different manner:

* All assignments will be graded on a ***Meets Specification***/***Does Not Yet Meet Specification*** basis, based on whether the student work meets the instructor-supplied specification.
* A minimum collection of assignments, indicating competency in the course learning outcomes, must be completed in a ***Meets Specification*** manner to earn a passing course grade (D).
* Higher passing grades (A, B, C) can be earned by completing more assignments and/or assignments that show higher-level thinking and learning skills.

### Assignment Grading

* All assignments in this course will be graded exclusively on a ***Meets Specification*** / ***Does Not Yet Meet Specification*** basis. 
* **For each assignment, you will be given a detailed specification explaining what is required for the work to be marked *Meets Specification*.** 
* Failing to meet ***any part*** of the specification will result in the work being marked **Does Not Yet Meet Specification**. 
* There will be no partial credit given. 
* If you are unclear on what the specification requires, it is your responsibility to ask me for clarification.
* It will be possible to revise and resubmit a limited number of assignments with **Does Not Yet Meet Specification** grades (see *Revision and Resubmission of Work* below).

### Course Grade Determination

Your grade for the course will be determined by which assignments and/or how many assignments you complete in a *Meets Specification* manner.

#### Base Grade

Assignment | Earn Base Grade<br>A | Earn Base Grade<br>B | Earn Base Grade<br>C | Earn Base Grade<br>D 
--- | :-: | :-: | :-: | :-:
Class Attendance and Participation | | | |
&nbsp;&nbsp;&mdash; Regular Classes<br>&nbsp;(out of 17 classes) | 16 | 15 | 14  | 13
&nbsp;&nbsp;&mdash; Final Project Work Classes<br>&nbsp;(out of 8 classes) | 4 | 3 | 2 | 1 
Assignments<br>&nbsp;(out of *n* assignments, where *n* &#8776; 8) |
&nbsp;&nbsp;&mdash; Base Assignment |  *n*<br>(&#8776;8) | *n*<br>(&#8776;8)| *n*<br>(&#8776;8) | *n* - 1<br>(&#8776;7)
&nbsp;&nbsp;&mdash; Intermediate "Add-On" |  int( *n* / 2 )<br>(&#8776;4) | int( *n* / 4 )<br>(&#8776;2)| 1 | 
&nbsp;&nbsp;&mdash; Advanced "Add-On" | int( *n* / 4 )<br>(&#8776;2)| 1 |  | 
Self-Directed Professional Development Blog Entries<br>&nbsp;(out of 10 weeks) | 5 | 4 | 3 | 2
Final Project Learning and Status Blog Entries<br>&nbsp;(out of 5 weeks) | 4 | 3 | 2 | 1
Final Project Proposal | &#10004; | &#10004; | &#10004; | &#10004;
Final Project Presentation | &#10004; | &#10004; | &#10004; | &#10004;
Final Project Documentation and Code | &#10004; | &#10004; | &#10004; | &#10004;
Exam Grade Average (2 exams) | > 50% | > 50% | > 50% | &le; 50%

* **Failing to meet the all the requirements for a particular letter grade will result in not earning that grade.** For example, even if you complete all other requirements for a B grade, but fail to write 4 "Self-Directed Reading Blog Entries" that *Meet Specification*, you will earn a C grade.
* **Failing to meet the all the requirements for earning a D grade will result in a failing grade for the course.**

#### Plus or Minus Grade Modifiers

* You will have a ***minus*** modifier applied to your base grade if the average of your exam grades is 65% or lower.
* You will have a ***plus*** modifier applied to your base grade if the average of your exam grades is 85% or higher.
* Each unused token remaining at the end of the semester can be used to increase the exam average by 2 percentage points.

*Notes:*
 
* WSU has no A+ grade.
* I reserve the right to revise *downward* the required number of assignments needed for each base grade due to changes in number of assignments assigned or unexpected difficulties with assignments.

## Attendance and Participation
### For Regular Classes
Because a significant portion of your learning will take place during the in-class activities and as part of your team, class attendance and participation are extremely important.

For your attendance and participation to be considered to *Meet Specification*, you must:

* Arrive on time and stay for the entire class session.
* Participate fully in the in-class activity:
    * Fulfill all responsibilities of your team role.
    * Contribute to the team's learning and answers to questions.
    * Work as part of the team on the activity (not on your own.)
    * Work on the in-class activity (not some other work.)

### For Final Project Work Classes
The Final Project Work Classes are important to provide you and your partner with a consistent, guaranteed time when you can meet to discuss the design of your application, troubleshoot development difficulties, and get advice and help from your instructor. Software development is not a solitary activity, and teams make more progess when they have regular synchronous meetings than they do with only irregular, asynchronous meetings.

For your attendance and participation to be considered to *Meet Specification*, you must:

* Arrive on time and stay for the entire class session.
* Work on your project (not some other work.)
* Work with your partner to coordinate work that will happen individually between class sessions.

## Assignments
The assignments will give you a chance to apply the material to different or larger tasks. The assignments will vary in what you will be asked to do - design, programming projects, written assignments, analysis, etc.

### Base Assignment
Every assignment will have a *base assignment* portion that must be completed for the assignment to be considered to *Meet Specification*.

* Anyone working to earn a grade of C or higher must submit work that *Meets Specification* for **all** Base Assignments.
*  Anyone working to earn a grade of D or higher must submit work that *Meets Specification* for **all but one** of the Base Assignments.

*A more complete specification for a "Meets Specification" (passing) Base Assignment will be given during with each assignment.*

### Intermediate "Add-On"
Most assignments will also have an *Intermediate Add-On* portion can be completed for anyone working for a course grade of C or higher. This will involve more detailed work on the same topic

* Differing numbers of Intermediate "Add-Ons" are required for different passing grades of C or higher. See the table under *Course Grade Determination*.

*A more complete specification for a "Meets Specification" (passing) Intermediate "Add-On" will be given during with each assignment.*

### Advanced "Add-On"
Most assignments will also have an *Advanced Add-On* portion can be completed for anyone working for a course grade of B or higher. This will involve even more detailed work on the same topic.

* Differing numbers of Advanced "Add-Ons" are required for different passing grades of B or higher. See the table under *Course Grade Determination*.

*A more complete specification for a "Meets Specification" (passing) Advanced "Add-On" will be given during with each assignment.*

## Reading Assignmnets
In addition, there wil be regular, assigned reading assignments. You are expected to complete these reading assignments by their assigned due dates, but you will not be checked on this. Completing these reading assignments will be needed to successfully complete the assignments and exam questions.

## Exams
We will have two exams. The exams will be completed outside of class time. The exams will be posted after class on a Wednesday, and will be due before class on the following Monday. You may complete the exam in any 2-hour period you wish during that time.

* Exam 1 is tentively scheduled to be posted after class on 7 October 2020, and due before noon on 12 October 2020.
* Exam 2 is tentively scheduled to be posted after class on 4 November 2020 and due before noon on 9 November 2020.

## Final Project and Presentation

For the last third (approximately) of the course, you will be working on a project of your own choice. This project will be designed and developed in pairs, using modeling techniques, design patterns, software architectures, and software frameworks from earlier in the course.

Final project presentations will be given during our scheduled final exam period: Wednesday, 16 December 2020 &mdash; 12:30-3:30pm

## Self-Directed Professional Development Blog
Two of the CS Program-Level Student Learning Outcomes that this course addresses are:

> * Learn new models, techniques, and technologies as they emerge and appreciate the necessity of such continuing professional development. (Mastery)
> * Communicate effectively both in written and oral form. (Mastery)

You will be required to read outside blogs, articles, and/or books; listen to podcasts; watch video tutorials/lectures; etc. on your own and keep a blog about those items that you found useful/interesting. Your blog must be publicly accessible[^1], and will be aggregated on the [CS@Worcester Blog](http://cs.worcester.edu/blog/).

* Differing numbers of blog entries are required for different passing grades. See the table under *Course Grade Determination*.
* I will only accept one blog entry per week. **You cannot wait until the end of the semester and then turn in all of your blog entries.** 

[^1]: If there is a reason why your name cannot be publicly associated with this course, you may blog under a pseudonym. You must see me to discuss the details, but your blog must still be publicly accessible and aggregated, and you must inform me of your pseudonym.

## Deliverables
All work will be submitted electronically through a variety of tools. The due date and time will be given on the assignment. The submission date and time will be determined by the submission timestamp of the tool used.

**Please do not submit assignments to me via email.** It is difficult for me to keep track of them and I often fail to remember that they are in my mailbox when it comes time to grade the assignment.

## Late Submissions
Late work will not be accepted. (See *Tokens* below.)

## Revision and Resubmission of Work

### For Assignments
If you receive a ***Does Not Yet Meet Specification*** on any portion of an Assignment (Base, Intermediate Add-On, or Advanced Add-On) you may revise and resubmit the assignment ***one time only*** without the use of a token.

* You must have submitted the original assignment on time, (or one day late with the use of a token.)
* You must submit your revision within one week from the date when the "Not Acceptable" grade and comments were posted in My Grades or GitLab. (You may use a token to submit the revision one day late.)
* You may ask me for clarification of the assignment, or of the comments I made on your submission.
* You may ask me to look at your revised solution to see if it addresses my comments.
* If you address all the comments in an acceptable fashion, your grade will be converted to ***Meets Specification***.
* You must let me know by email when you resubmit the assignment, so that I know to regrade it.

### For Self-Directed Professional Development Blog Entries.
You may revise and resubmit the ***first*** blog entry on which you receive a ***Does Not Yet Meet Specification*** without the use of a token.

* You must have submitted the original blog entry on time, (or one day late with the use of a token.)
* You must submit your revision within one week from the date when the ***Does Not Yet Meet Specification*** grade and comments were posted in My Grades. (You may use a token to submit the revision one day late.)
* You may ask me for clarification of the comments I made on your blog entry.
* You may ask me to look at your revised blog entry to see if it addresses my comments.
* If you address all the comments in an acceptable fashion, your grade will be converted to ***Meets Specification***.
* You must let me know by email when you have posted the revised blog entry (**with the URL to the revised blog entry**), so that I know to regrade it.

## Tokens
Each student will be able to earn up to 5 tokens over the course of the semester. These tokens will be earned by completing simple set-up and housekeeping tasks for the course.

Each token can be used to:

* replace a single missed class session (up to a maximum of 3 missed class sessions)
* turn in an assignment late by 24 hours
* turn in a second blog entry in an a single week
* revise and resubmit an assignment a second time. Any work to be revised and resubmitted must have been submitted by the original due date.
* Each unused token remaining at the end of the semester can be used to increase the exam average by 2 percentage points.


### Token Accounting
* Unused tokens will be kept track of in the Blackboard *My Grades* area.
* Tokens will not be automatically applied. You must explicitly tell me **by email** when you want to use a token, and for which assignment.

## Getting Help
If you are struggling with the material or a project please see me as soon as possible. Often a few minutes of individual attention is all that is needed to get you back on track.

By all means, try to work out the material on your own, but ask for help when you cannot do that in a reasonable amount of time. The longer you wait to ask for help, the harder it will be to catch up. 

**Asking for help or coming to see me during office hours is not bothering or annoying me. I am here to help you understand the material and be successful in the course.**

## Contacting Me
You may contact me by email (Karl.Wurst@worcester.edu), telephone (+1-508-929-8728), or see me in my office. My office hours are listed on the schedule on my web page ([http://cs.worcester.edu/kwurst/](http://cs.worcester.edu/kwurst)) or you may make an appointment for a mutually convenient time.

**If you email me, please include “[CS-343]” in the subject line, so that my email program can correctly file your email and ensure that your message does not get buried in my general mailbox.**

**If you email me from an account other than your Worcester State email, please be sure that your name appears somewhere in the email, so that I know who I am communicating with.** 

<span title="http://www.phdcomics.com/comics.php?f=1795"><a href="http://www.phdcomics.com/comics.php?f=1795"><img src="http://www.phdcomics.com/comics/archive/phd042215s.gif" alt="Cartoon with bad examples of how to send email to your instructor."></a></span>

My goal is to get back to you within 24 hours of your email or phone call (with the exception of weekends and holidays), although you will likely hear from me much sooner. If you have not heard from me after 24 hours, please remind me.

## Code of Conduct/Classroom Civility
All students are expected to adhere to the policies as outlined in the University's Student Code of Conduct.

## Student Responsibilities 

* Contribute to a class atmosphere conducive to learning for everyone by asking/answering questions, participating in class discussions. Don’t just lurk!
* When working with a partner, participate actively. Don't let your partner do all the work - you won't learn anything that way.
* Seek help when necessary
* Start assignments as soon as they are posted.  Do not wait until the due date to seek help/to do the assignments
* Make use of the student support services (see below)
* Expect to spend at least 9 hours of work per week on classwork.
* Each student is responsible for the contents of the readings, handouts, and homework assignments.

## Accessibility Statement
Worcester State University values the diversity of all of our students, faculty and staff.  We recognize the importance of each student’s contribution to our campus community.  WSU is committed to providing equal access and support to all qualified students through the provision of reasonable accommodations so that each student may fully participate in programs and services at WSU.  If you have a disability that requires reasonable accommodations, please contact Student Accessibility Services at SAS@worcester.edu or 508-929-8733.  Please be aware that accommodations cannot be enacted retroactively, making timeliness a critical aspect for their provision.

## Tutoring Services/Academic Success Center
Tutoring Services are offered through the Academic Success Center (ASC).  The ASC is located on the first floor of the Administration building, A-130.  Tutoring services are provided to students FREE of charge.  Students seeking academic assistance should visit the center as soon as possible or contact the Tutoring Coordinator at 508-929-8139

## The Math Center
The Math Center provides free assistance to students in Mathematics.  It is located on the first floor of the Sullivan Academic Building, S143.

## The Writing Center
The writing center provides free assistance to students in the areas of research and writing.  It is located on the third floor of the Sullivan Academic Building, S306.  To schedule an appointment, please call 508-929-8112 or email the Center at writingcenter@worcester.edu.  To find out more information about the Writing Center including the Center's hours and the Center's Online Writing Lab, visit their website at <a href="http://www2.worcester.edu/WritingCenter/" target="_blank">http://www2.worcester.edu/WritingCenter/</a>.

## Worcester State Library
For research help, go to the Library! The [Worcester State University Library] (http://www.worcester.edu/library), located on the second and third floors of the Learning Resource Center, provides access to print materials and items on course reserve in addition to a wide variety of full-text online resources accessible both on- and off-campus, including e-books, journal articles, newspapers, and magazines. Articles and book chapters not available in full-text through the Library’s resources can be obtained through Interlibrary Loan (ILL). With a little planning, ILL expands your ability to get credible sources about topics you pursue in your coursework. Additionally, as a member library of the [ARC consortium](http://www.worcesterarc.org), WSU students can also access resources at other college libraries.

The librarians at WSU can help you develop research questions, identify research strategies, search for relevant and reliable information and data, select the best sources for your paper or project, and cite that information. A librarian can visit your class, meet with you in person (one-on-one or in groups), and provide assistance via email, phone, or chat. While every WSU Librarian is able to help students with a project in any discipline, each department has a designated librarian to support the research needs of the students in that department. You are welcome to stop by their office, or make an appointment! [http://libguides.worcester.edu/askus](http://libguides.worcester.edu/askus)

## Academic Conduct
Each student is responsible for the contents of the readings, discussions, class materials, textbook and handouts. All work must be done independently unless assigned as a group project. You may discuss assignments and materials with other students, but you should never share answers or files. **Everything that you turn in must be your own original work, unless specified otherwise in the assignment.**

Students may help each other understand the programming language and the development environment but students may not discuss actual solutions, design or implementation, to their programming assignments before they are submitted or share code or help each other debug their programming assignments. The assignments are the primary means used to teach the techniques and principles of computer programming; only by completing the programs individually will students receive the full benefit of the assignments. If you are looking at each other’s code before you submit your own, you are in violation of this policy. 

Students may not use solutions to assignments from any textbooks other than the text assigned for the course, or from any person other than the instructor, or from any Internet site, or from any other source not specifically allowed by the instructor. If a student copies code from an unauthorized source and submits it as a solution to an assignment, the student will receive a 0 for that assignment.

**Any inappropriate sharing of work or use of another's work without attribution will result in a grade of zero on that assignment for all parties involved. If you do so a second time, you will receive an “E” for the course.**

Academic integrity is an essential component of a Worcester State education. Education is both the acquisition of knowledge and the development of skills that lead to further intellectual development. Faculty are expected to follow strict principles of intellectual honesty in their own scholarship; students are held to the same standard. Only by doing their own work can students gain the knowledge, skills, confidence and self-worth that come from earned success; only by learning how to gather information, to integrate it and to communicate it effectively, to identify an idea and follow it to its logical conclusion can they develop the habits of mind characteristic of educated citizens. Taking shortcuts to higher or easier grades results in a Worcester State experience that is intellectually bankrupt.

Academic integrity is important to the integrity of the Worcester State community as a whole. If Worcester State awards degrees to students who have not truly earned them, a reputation for dishonesty and incompetence will follow all of our graduates. Violators cheat their classmates out of deserved rewards and recognition. Academic dishonesty debases the institution and demeans the degree from that institution.  

It is in the interest of students, faculty, and administrators to recognize the importance of academic integrity and to ensure that academic standards at Worcester State remain strong. Only by maintaining high standards of academic honesty can we protect the value of the educational process and the credibility of the institution and its graduates in the larger community.

**You should familiarize yourself with Worcester State College’s Academic Honesty policy. The policy outlines what constitutes academic dishonesty, what sanctions may be imposed and the procedure for appealing a decision. The complete Academic Honesty Policy appears at: [http://www.worcester.edu/Academic-Policies/](http://www.worcester.edu/Academic-Policies/)**

**If you have a serious problem that prevents you from finishing an assignment on time, contact me and we'll come up with a solution.**

## Student Work Retention Policy
It is my policy to securely dispose of student work one calendar year after grades have been submitted for a course.

## Schedule
**This schedule is subject to change.**

Week | Monday | Wednesday
:-: | :-: | :-:
1 | 31 August &mdash; **No Class**<br>**Classes have not started yet** | 2 September<br>Team Roles and Grading Schemes, Syllabus,<br> Review: Object-Oriented Design Principles
2 | 7 September &mdash; **No Class**<br>**Labor Day** | 9 September<br>Review: Object-Oriented Design Principles
3 | 14 September<br>Modeling in UML: Class Diagrams | 16 September<br>Modeling in UML: Use Cases, Activity Diagrams
4 | 21 September<br>Design Patterns | 23 September<br>Design Patterns
5 | 28 September<br>Code Smells and Anti-Patterns | 30 September<br>SOLID Principles
6 | 5 October<br>Software Architectures | 7 October<br>Containerization<br>**Exam 1 Posted**
7 | 12 October &mdash; **No Class**<br>**Columbus Day**<br>**Exam 1 Due** | 14 October<br>Microservice Architecture
8 | 19 October<br>Introduction to REST | 21 October<br>Designing and Implementing REST APIs
9 | 26 October<br>Data Persistence Layer | 28 October<br>Front End Design and Implementation
10 | 2 November<br>Front End Design and Implementation | 4 November<br>Final Project Planning<br>**Exam 2 Posted**
11 | 9 November<br>Final Project Work<br>**Exam 2 Due** | 11 November &mdash; **No Class**<br>**Veterans Day**
12 | 16 November<br>Final Project Work | 18 November<br>Final Project Work 
13 | 23 November<br>Final Project Work | 25 November &mdash; **No Class**<br>**Thanksgiving Recess**
14 | 30 November<br>Final Project Work | 2 December<br>Final Project Work
15 | 7 December<br>Final Project Work | 9 December<br>Final Project Work
Finals | 14 December &mdash; **No Class**<br>Final Exams | 16 December &mdash; **Final Presentations**<br>**12:30-3:30pm**


&copy; 2020 Karl R. Wurst, <kwurst@worcester.edu>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.